import axios from 'axios';

export default axios.create({
	baseURL: 'https://api.yelp.com/v3/businesses',
	headers: {
		Authorization: 'Bearer 5gQ6hUA8U9ZlYZyTeR-xOjW03XVz7fGEFA75KOZqGmBaUKaijM7lmYT2LTNCHPBC4z6GyZnynbdnb5oHcCpdrCrOzevAVBl__3QdaHwwi7dd-Vb3m10N7-ENdf9MXnYx'
	}
});