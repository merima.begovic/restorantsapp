import React, { useState } from 'react';
import {
	View, Text, StyleSheet, ScrollView
} from 'react-native';

import SearchBar from '../components/SearchBar';
import ResultsList from '../components/ResultsList';

import useResults from '../hooks/useResults';

const SearchScreen = () => {
	const [term, setTerm] = useState('');
	const [searchApi, results, error] = useResults();

	const filterResultsByPrice = (price) => {
		return results.filter(res => res.price === price);
	};

	return (
		<>
			<SearchBar
				term={term}
				onTermChange={setTerm}
				onTermSubmit={() => searchApi(term)}
			/>
			{error ? <Text>{error}</Text> : null}
			<ScrollView>
				<ResultsList
					title="Cost Effective"
					results={filterResultsByPrice('$')}
				/>
				<ResultsList title="Bit Pracier"
					title="Cost Effective"
					results={filterResultsByPrice('$$')}	
				/>
				<ResultsList title="Big Spender"
					title="Cost Effective"
					results={filterResultsByPrice('$$$')}				
				/>
			</ScrollView>
		</>

	);
};

const styles = StyleSheet.create({
	
});

export default SearchScreen;