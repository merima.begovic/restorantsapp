import React, { useState, useEffect } from 'react';
import yelp from '../api/yelp';

import {
	View, Text, StyleSheet, Image, FlatList
} from 'react-native';


const ResultsShowScreen = ({ navigation }) => {
	const id = navigation.getParam('id');

	const [result, setResult] = useState({});

	const getResult = async (id) => {
		const response = await yelp.get(`/${id}`);
		setResult(response.data);
	};

	useEffect(() => {
		getResult(id);
	}, []);

	if (!result) {
		return null;
	}

	return (
		<View style={styles.container}>
			<Text style={styles.titleStyle}>
				{result.name}
			</Text>
			<FlatList
				data={result.photos}
				keyExtractor={photo => photo}
				renderItem={({ item }) => {
					return (
						<Image 
							source={{ uri: item }}
							style={styles.imageStyle}
						/>
					);
				}}
			/>
		</View>
	)
};

const styles = StyleSheet.create({
	container: {
		marginLeft: 15,
	},
	imageStyle: {
		width: 250,
		height: 120,
		borderRadius: 4,
		marginBottom: 5,
	},
	titleStyle: {
		fontWeight: 'bold',
	},
});

export default ResultsShowScreen;