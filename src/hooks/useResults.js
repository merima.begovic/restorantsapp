import { useEffect, useState } from 'react';
import yelp from '../api/yelp';

export default  () => {
	const [results, setResults] = useState([]);
	const [error, setError] = useState('');

	const searchApi = async (searchTerm) => {
		try {
			const res = await yelp.get('/search', {
				params: {
					term: searchTerm,
					limit: 50,
					location: 'san jose',
				}
			});
			// console.log('res', res.data.businesses);
			setResults(res.data.businesses);
		} catch (err) {
			// console.log(err);
			setError('Something went wrong.')
		}
	};

	useEffect(() => {
		searchApi('pasta');
	}, []);

	return [searchApi, results, error];
};